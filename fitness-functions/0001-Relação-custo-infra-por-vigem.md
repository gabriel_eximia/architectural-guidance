# Relação custo infra por vigem otimizada

### Responsables

- Gabriel S. Kohlrausch (gabriel@eximia.co)

## Context

Custos de infra devem ser proporcionais a quantidade de viagens otimizadas pelo serviço.

## Metrics

## Measurements

| Data                             | Valor     | Meta  |
|----------------------------------|-----------|-------|
| 2022/02/15                       | x         | x     |
