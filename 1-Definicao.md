# Definição de Arquitetura

Embora não exista consenso absoluto sobre o conceito, alguns aspectos são amplamente aceitos. **Qualquer discussão madura sobre a arquitetura de um software aborda, de maneira mais ou menos qualificada, seus componentes, a responsabilidade destes e a forma como eles se relacionam**.

A arquitetura de software tampouco é estática, ou seja, a arquitetura evolui na mesma medida em que os objetivos de negócio podem ser redefinidos, restrições são revisadas e atributos de qualidade determinados.

Há sempre, pelo menos, duas visões a respeito da arquitetura. A primeira, trata da arquitetura como ela está (AS-IS). A segunda, projeta como a arquitetura deverá ser (TO-BE). Seja, para adequar aos novos tempos, seja para compensar dívidas técnicas.

Quanto mais explícitas forem as condições atuais (AS-IS) e mais explícitas forem as expectativas para o futuro (TO-BE), mais fácil será planejar a evolução arquitetural através de “arquiteturas intermediárias”.

## Design de software

Quando falamos sobre arquitetura, invariavelmente falamos sobre design de software. Entretanto, o oposto não é verdadeiro. **As decisões sobre design que são parte da arquitetura de um software são aquelas que tem relação direta com o atendimento dos objetivos do negócio, respeito a restrições e atendimento dos atributos de qualidade.**

A maioria das decisões de design são individuais, em conformidade com os acordos estabelecidos pelo time de desenvolvimento e acontecem o tempo todo. Algumas decisões de design relacionadas com a arquitetura, por outro lado, têm impacto maior e são menos frequentes. **Decisões arquiteturais raramente são produtos do trabalho individual e geralmente são mais difíceis e custosas de revisitar.**

