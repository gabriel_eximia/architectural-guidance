# Estratégia

Estratégia é um padrão coerente para tomada de decisões. Ou seja, quando todas as pessoas de um grupo tomam decisões seguindo um mesmo padrão coerente, obedecendo um conjunto estável de critérios, diz-se que estão alinhados a uma mesma estratégia – mesmo quando, eventualmente, as decisões em si sejam diferentes.

Quando um time de desenvolvimento adota boas práticas de arquitetura, seus membros seguem uma estratégia comum para suas escolhas e isso é extremamente importante. Afinal, para cada problema tecnológico há uma continuidade crescente de respostas viáveis – com prós e contras – que dificultam o consenso. **A alternativa selecionada deve ser aquela que for mais aderente aos objetivos de negócio, restrições e atributos de qualidade**

As melhores práticas de arquitetura de software invariavelmente acionam as partes interessadas (stakeholders) para explicitar e atualizar os objetivos do negócio que precisam ser atingidos, focando nos resultados e não na forma. Elas também devem diagnosticar, até o último momento responsável, as restrições que devem ser observadas e assegurar que elas sejam respeitadas. Finalmente, também devem inferir os atributos de qualidade que garantem a eficiência no atendimento do negócio e das restrições.

Assim, podemos dizer que software bem-feito atende os **objetivos do negócio**, respeitando **restrições** e atingindo **atributos de qualidade**.

## Restrições Mapeadas

| RESTRIÇÃO                                         | STATUS     |
|---------------------------------------------------|------------|
| Time com pouca experiência em projetos complexos  | PRELIMINAR |
| Time focado em desenvolvimento APEX               | PRELIMINAR |
| Time pequeno                                      | PRELIMINAR |
| Ser compliance a LGPD                             | PRELIMINAR |
| Permitir gestão de APIs keys para APIs públicas   | PRELIMINAR |
| Intgração de processos fiscais com Totvs Protheus | PRELIMINAR |

## Atributos de Qualidade Mapeados

| Atributo de qualidade                                                             | STATUS     |
|-----------------------------------------------------------------------------------|------------|
| Tempo de resposta no máximo 300ms no busca de ofertas                             | PRELIMINAR |
| Tempo de reposta de no máximo 500ms para endpoints críticos                       | PRELIMINAR |
| Mínimo de 80% de cobertura de testes de regressão no caminho crítico da aplicação | PRELIMINAR |
| Suportar XX K ofertas e XX K por mês com disponibilidade de 99.99%                | PRELIMINAR |
| Dívida técnica não deve ser superior a 10% do backlog                             | PRELIMINAR |
| 100% de processos críticos auditados                                              | PRELIMINAR |
| 100% de Regras de negócio configuráveis via UI                                    | PRELIMINAR |
| Observabilidade em 100% da aplicação                                              | PRELIMINAR |
| 100% das aplicações realizando logs centralizados e estruturados                  | PRELIMINAR |
| Acoplamento com Totvs Protheus apenas em processos fiscais                        | PRELIMINAR |

## Objetivos de Negócio Mapeados

| Objetivos                                                                                                      | STATUS     |
|----------------------------------------------------------------------------------------------------------------|------------|
| Permitir que empresas criem ofertas de viajens para transportadores lançarem propostas e realizarem as viajens | PRELIMINAR |
| Crescimento sustentável de infra-estrutura conforme volume de viagens                                          | PRELIMINAR |
| Reduzir o lead time para novas features                                                                        | PRELIMINAR |