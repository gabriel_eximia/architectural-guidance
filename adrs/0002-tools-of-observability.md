# Tools of Observability

## Status

**Proposed** -> 2022/02/15

### Responsables

- Gabriel S. Kohlrausch (gabriel@eximia.co)

## Context

A MatrixCargo implementa os seguintes padrões arquiteturais:
1. Pipes and Filters[^1]
2. Service-Based-Architecture[^2]
3. Architecture Distributed in Services EDA[^3]

Dado que estes estilos arquiteturais tem como base computação distríbuida[^4], ratrear um evento entre sistemas distribuídos até sua origem é uma tarefa complexa.

As técnicas e ferramentas convencionais de monitoramento lutam para rastrear os muitos caminhos de comunicação e interdependências nessas arquiteturas distribuídas, dificultando a detecção e investigação de problemas.

## Decision

Observabilidade permite que times de devops ou SRE monitorem os sistemas modernos com mais eficiência auxiliando a encontrar e conectar efeitos em uma cadeia complexa rastreando-os até sua causa. Além disso, oferece uma visibilidade de toda a sua arquitetura sendo um componente chave para auxiliar a mitigar problemas nos sistemas. 

Os tipos de dados que deve agregar à observabilidade do sistema são[^5]:

- Métricas
- Logs
- Traces

### Ferramentas avaliadas

Com base no relatório do Gartner[^6] que pontua as principais ferramentas de observabilidade, as opções analisadas serão:
- Datadog
- Elastic
- Dynatrace

[TODO] Fazer avaliação das ferramentas

## Consequences

[TODO] Fazer Prós e contras

## Status log
2022/02/15 - Proposed by Eximia.


## Notes and citations
[^1] [Pipes and filters](https://docs.microsoft.com/pt-br/azure/architecture/patterns/pipes-and-filters): Decompor uma tarefa que executa processamento complexo em uma série de elementos separados que podem ser reutilizados. Isso pode melhorar o desempenho, escalabilidade e reutilização, permitindo que os elementos de tarefa que executam o processamento sejam implantados e escalados de forma independente.

[^2] [Service-Based-Architecture](https://www.infoq.com/news/2016/10/service-based-architecture)

[^3] [Event Driven Architecture](https://www.gartner.com/en/information-technology/glossary/eda-event-driven-architecture)

[^4] Entendendo os principais desafios da [computação distribuída](http://www.psinaptic.com/link_files/distributed_computing.pdf)

[^5] [Observabilidade](https://www.linkedin.com/pulse/observability-preemptive-approach-problem-solving-seshachala/)

[^6] [Magic Quadrant for Application Performance Monitoring](https://www.gartner.com/doc/reprints?id=1-25SQ95K7&ct=210414&st=sb)