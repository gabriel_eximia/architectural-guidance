# ADRs (Architectural Decision Records)

Uma decisão arquitetural trata sempre de uma escolha de design relacionada a uma característica funcional ou não-funcional  indiscutivelmente relevante, pois, geralmente, tem algumas das seguintes características:

- afeta os objetivos do negócio ou atributos de qualidade (como performance, disponibilidade, segurança, manutenabilidade, etc.)
- é difícil de ser desfeita (uma das quatro fontes da complexidade)
- implica em gastos ou economias consideráveis de tempo ou dinheiro (protip: tempo geralmente é um proxy para dinheiro)
- demandou, para sua formulação, considerável tempo e esforço do time, geralmente demandando provas de conceito e avaliação de trade-offs.
- é extremamente complexa podendo não fazer sentido em primeira análise, sem o background necessário.

## Template

[Modelo de ADR](adrs/template.md)

## ADRs do projeto

| ADR                                                                                                      | STATUS     |
|----------------------------------------------------------------------------------------------------------|------------|
| [0001-messages-broker](adrs/0001-messages-broker.md)                                                     | Proposed   |
| [0002-tools-of-observability](adrs/0002-tools-of-observability.md)                                       | Proposed   |
| [0003-linguagem-backend](adrs/0003-linguagem-backend.md)                                                 | Proposed   |
| [0004-linguagem-frontend](adrs/0004-linguagem-frontend.md)                                               | Proposed   |
| [0005-banco-dados](adrs/0005-banco-dados.md)                                                             | Proposed   |
| [0006-banco-dados](adrs/0006-banco-dados.md)                                                             | Proposed   |
| [0007-microfornt-step1](adrs/0007-microfornt-step1.md)                                                   | Proposed   |
| [0008-api-gateway-step1](adrs/0008-api-gateway-step1.md)                                                 | Proposed   |
| [0009-estrangulamento-serviço-comunicação-step1](adrs/0009-estrangulamento-serviço-comunicação-step1.md) | Proposed   |
| [0010-serviço-anti-corrupção-totvs-step1](adrs/0010-serviço-anti-corrupção-totvs-step1.md)               | Proposed   |
| [0011-estrangulamento-serviço-ofertas-step1](adrs/0011-estrangulamento-serviço-ofertas-step1.md)         | Proposed   |
